/*implementation of Quicksort <https://en.wikipedia.org/wiki/Quicksort#Hoare_partition_scheme>*/
#include <iostream>
#include <string>

/*function declerations*/
template<typename T> void qs(T data[], const std::size_t lo, const std::size_t hi);
template<typename T> std::size_t partition(T data[], const std::size_t lo, const std::size_t hi);
short test_qs();
template<typename T>  bool testCase(T test[], const std::size_t len);
template<typename T> bool checkAscending(const T data[], const std::size_t len);
template<typename T> void printArr(T arr[], std::size_t len, char pre='\0', char post='\0');
template<typename T> T pickPivot(const T a, const T b, const T c);
template<typename T> void insertionSort(T data[], const std::size_t lo, const std::size_t hi);

/*overload relational operators*/
bool operator > (std::string A, std::string B);
bool operator < (std::string A, std::string B);


int main()
{
	auto errors = test_qs();
	std::cout << "Errors found in test cases: " << errors << std::endl;
	return errors;
}

template<typename T> void qs(T data[], const std::size_t lo, const std::size_t hi)
{
	if(lo >= 0 && hi >= 0 && lo < hi)
	{
		if(hi - lo <= 10)//use insertion sort for subarrays size 10 or less
		{
			insertionSort(data, lo, hi);
		}
		else
		{
			const auto p = partition(data, lo, hi);
			qs(data, lo, p);
			qs(data, p + 1, hi);
		}
	}
}

/*using Hoare's partition scheme*/
template<typename T> std::size_t partition(T data[], const std::size_t lo, const std::size_t hi)
{
	const T pivot = pickPivot(data[lo], data[hi], data[lo+(hi-lo)/2]);
	auto i = lo - 1;//left index
	auto j = hi + 1;//right index
	for(;;)
	{
		do
		{
			i++;
		}while(data[i] < pivot);
		
		do{
			j--;
		}while(data[j] > pivot);
		
		if(i >= j)
		{
			return j;
		}
		
		std::swap(data[i], data[j]);
	}
}


short test_qs()
{
	
	
	int testCase1[11] = {4,76,34,4,56,4,3,12,0,-10,4};//random
	int testCase2[12] = {10,15,34,-83,4,56,4,9,1,0,-10,40};//random
	int testCase3[6] = {1,2,3,4,5,6};//allready sorted
	int testCase4[6] = {6,5,4,3,2,1};//reverse sorted
	int testCase5[8] = {1,2,3,4,5,6,8,7};//one inversion
	int testCase6[30] = {2,5,7,9,4,-2,5,8,9,6,-44,5,66,23,58,52,34,78,-23,80,10,12,1,2,3,4,5,7,9,10};//many terms
	long long testCase7[5] = {-300000LL, 10000000LL, 2000LL, 2000000LL, 300000000LL};
	float testCase8[5] = {1.0f,2.0f,3.0f,4.1f,4.5f};
	float testCase9[7] = {2.3f,5.2f,1.2f,-4.1f,-5.2f,-5.3f,5.3f};
	unsigned int testCase10[45] = {1,2,3,4,5,6,7,8,9,5,9,8,76,5,4,3,2,1,10,12,43,22,12,13,34,54,32,11,31,3,5,6,7,8,6,4,3,2,4,7,6,23,43,23,45};
	short testCase11[2] = {-5, -3};//few terms
	std::string testCase12[12] = {"aaa", "bbb", "ddd", "ccc", "fff", "eee", "zzz", "gaa", "gbb", "hhh", "haa", "hbb"};


	short errors = 0;
	if(!testCase(testCase1, sizeof(testCase1)/sizeof(*testCase1))) errors++;
	if(!testCase(testCase2, sizeof(testCase2)/sizeof(*testCase2))) errors++;
	if(!testCase(testCase3, sizeof(testCase3)/sizeof(*testCase3))) errors++;
	if(!testCase(testCase4, sizeof(testCase4)/sizeof(*testCase4))) errors++;
	if(!testCase(testCase5, sizeof(testCase5)/sizeof(*testCase5))) errors++;
	if(!testCase(testCase6, sizeof(testCase6)/sizeof(*testCase6))) errors++;
	if(!testCase(testCase7, sizeof(testCase7)/sizeof(*testCase7))) errors++;
	if(!testCase(testCase8, sizeof(testCase8)/sizeof(*testCase8))) errors++;
	if(!testCase(testCase9, sizeof(testCase9)/sizeof(*testCase9))) errors++;
	if(!testCase(testCase10, sizeof(testCase10)/sizeof(*testCase10))) errors++;
	if(!testCase(testCase11, sizeof(testCase11)/sizeof(*testCase11))) errors++;
	if(!testCase(testCase12, sizeof(testCase12)/sizeof(*testCase12))) errors++;
	

	return errors;
	
}

/*return false if error in sorting, true otherwise*/
template<typename T>  bool testCase(T test[], const std::size_t len)
{
	qs(test, 0, len - 1);
	if(!checkAscending(test, len - 1))
	{
		std::cerr << "Error:\n";
		printArr(test, len, '\t', '\n');
		return false;
	}
	
	return true;
}

/*Return true if data is sorted in ascending order, false otherwise*/
template<typename T> bool checkAscending(const T data[], const std::size_t len)
{
	if(len < 2)
	{
		return true;
	}
	for(std::size_t i = 0; i < len - 1; i++)
	{
		if(data[i] > data[i + 1])
		{
			return false;
		}
	}
	return true;
}

/*Prints contents of array to standard output. Optionally specify start and end characters for delamination (e.g. new line).*/
template <typename T> void printArr(T arr[], std::size_t len, char pre, char post)
{
	std::cout << pre;
	for(std::size_t i = 0; i < len - 1; i++)
	{
		std::cout << arr[i] << ", ";
	}
	std::cout<< arr[len - 1];
	std::cout << post;
}

/*pick the middle (median) value*/
template<typename T> T pickPivot(const T a, const T b, const T c)
{
	//credit to user "caiohamamura" for this strategy of picking median value <https://stackoverflow.com/questions/7559608/median-of-three-values-strategy>
    if ((a > b) ^ (a > c))//check if a exclusivley great than b or c
	{
        return a;
	}
    else if ((b > a) ^ (b > c))
	{
        return b;
	}
    else
	{
        return c;
	}
}

/*sort portion of array from lo to hi, inclusively*/
template<typename T> void insertionSort(T data[], const std::size_t lo, const std::size_t hi)
{	
	for(std::size_t i = lo + 1; i <= hi; i++)
	{
		const auto x = data[i];
		std::size_t j;
		for(j = i - 1; j + 1  > lo && data[j] > x; j--)
		{
			data[j+1] = data[j];
		}
		data[j+1] = x;
	}
}

/*operator > is already overloaded for std::string but this serves as example of how to overload an operator for an object that isn't already supported*/
bool operator > (std::string A, std::string B)
{
	if(A.compare(B) > 0)
	{
		return true;
	}
	return false;

}

bool operator < (std::string A, std::string B)
{
	if(A.compare(B) < 0)
	{
		return true;
	}
	return false;
}