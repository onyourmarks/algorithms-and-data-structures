/*Implementation of the max binary heap data structure: <https://en.wikipedia.org/wiki/Binary_heap> 
First index is unused as to simplify calculations*/
#include <stdexcept>
#include <cmath>
#include <iostream>
#include "MaxHeap.h"

/*create heap from vector*/
void MaxHeap::buildHeap(std::vector<int> data)
{
	heap = {-1};//add dummy value for first element
	heap.insert(heap.end(), data.begin(), data.end());
	
		
	//iterate through internal nodes from bottom up
	for(std::size_t node = (heap.size() / 2); node > 0; node--)
	{
		siftDown(node);
	}
}

		
/*helper function to constructor*/
void MaxHeap::siftDown(const std::size_t node)
{
	std::size_t largest = node;
	const std::size_t leftChild = leftChildIndex(node);
	const std::size_t rightChild = rightChildIndex(node);
	if(leftChild < heap.size() && heap[leftChild] > heap[largest])
	{
		largest = leftChild;
	}
	if(rightChild < heap.size() && heap[rightChild] > heap[largest])
	{
		largest = rightChild;
	}
	if(largest != node)
	{
		std::swap(heap[node], heap[largest]);
		siftDown(largest);//sub-tree may need to have heap property restored after change
	}
}

/*formulas for parent and children <https://stackoverflow.com/questions/22900388/why-in-a-heap-implemented-by-array-the-index-0-is-left-unused>*/
std::size_t MaxHeap::parentIndex(const std::size_t node)
{
	return (node/ 2);
}
		
std::size_t MaxHeap::leftChildIndex(const std::size_t node)
{
	return (2 * node);
}
		
std::size_t MaxHeap::rightChildIndex(const std::size_t node)
{
	return (2 * node + 1);
}
		
/*Return true if data is a binary max heap. Traversal of array takes place as breadth first.
  Special cases: if only one node then it is a heap, if zero nodes and exception is thrown
  startingIndex is useful because some heaps start at index 1 (simpler calculations) and others 0 (saves a node)*/
bool MaxHeap::isHeap(std::vector<int> data, const std::size_t startingIndex /* = 0 */)
{
	if(data.empty())
	{
		throw std::out_of_range("Heap is empty");
	}
	if(startingIndex > 1)
	{
		throw std::invalid_argument("Starting index must be 0 or 1");
	}
	if(data.size() == startingIndex + 1)
	{
		return true;
	}
	if(startingIndex == 0)
	{
		data.insert(data.begin(), data[0]);//add a dummy value to front by copying whatever value is first
	}

	for(std::size_t i = 1; i < data.size()/2; i++)//iterate up to last internal node in tree: there are floor(n/2) internal nodes in complete tree with index starting at 1
	{
		if(data.at(i) < data.at(leftChildIndex(i)))
		{
			std::cout << "not heap b/c " << data.at(i) << " (index " << i << ") " << " < " << data.at(leftChildIndex(i)) << " (index " << leftChildIndex(i) << ") "  << "\n";
			return false;
		}
		
		if(rightChildIndex(i) < data.size())
		{
			if(data.at(i) < data.at(rightChildIndex(i)))
			{
				std::cout << "not heap b/c " << data.at(i) << " (index " << i << ") " << " < " << data.at(rightChildIndex(i)) << " (index " << rightChildIndex(i) << ") "  << "\n";
				return false;
			}
		}
	}
	return true;
}

std::vector<int> MaxHeap::toVector()
{
	return std::vector<int>(heap.begin() + 1, heap.end());;
}

int MaxHeap::extractMax()
{
	if(heap.empty())
	{
		throw std::out_of_range("Extraction failed, heap is empty");
	}
	
	if(heap.size() == 2)
	{
		const auto single = heap.at(1);
		heap.pop_back();
		return single;
	}
	
	const auto max = heap.at(1);
	heap[1] = heap.back();
	heap.pop_back();
	siftDown(1);
	return max;
}

void MaxHeap::insert(const int nodefindMe)
{
	heap.push_back(nodefindMe);
	siftUp(heap.size() - 1);
}

void MaxHeap::siftUp(std::size_t nodeIndex)
{
	while(nodeIndex > 1 && heap.at(parentIndex(nodeIndex)) < heap.at(nodeIndex))
	{
		std::swap(heap[nodeIndex], heap[parentIndex(nodeIndex)]);
		nodeIndex = parentIndex(nodeIndex);
	}
}

/*return true if findMe found*/
bool MaxHeap::linearSearch(const int findMe)
{
	for(std::size_t i = 1; i < heap.size(); i++)
	{
		if(heap[i] == findMe)
		{
			return true;
		}
	}
	return false;
}

/*return true if findMe found
Optimization used: if node has less value than findMe, no need to continue searching its subtree
Works by traversing down the heap only visiting left children, then works way back up iterating through right siblings' subtrees*/
bool MaxHeap::recursiveSearch(const int findMe, std::size_t i)
{
	if(heap[i] == findMe)
	{
		return true;
	}
	const auto startingIndex = i;	
	while(leftChildIndex(i) < heap.size())
	{
		i = leftChildIndex(i);
		if(heap[i] == findMe)
		{
			return true;
		}
		if(heap[i] < findMe)
		{
			break;
		}
		
	}
	
	if(i != startingIndex)//this check is to prevent infinite recursion when a sibling is recursed on that is a leaf
	{
		i = parentIndex(i);
			if(rightChildIndex(i) < heap.size())
			{
				if(leftChildIndex(rightChildIndex(i)) < heap.size())//testing if right child is not a leaf
				{
					if(recursiveSearch(findMe, rightChildIndex(i)))
					{
						return true;
					}
				}
				else//node is a leaf
				{
					if(heap[rightChildIndex(i)] == findMe)
					{
						return true;
					}
				}
			}
	}

	
	while(i != startingIndex)//code is this block is simillar to above if statment
	{
		i = parentIndex(i);
		
			if(recursiveSearch(findMe, rightChildIndex(i)))
			{
				return true;
			}
		
	}
			
	
	return false;
}

/*return true if findMe found
Optimization used: if node has less value than findMe, no need to continue searching its subtree
Works by node checking its own value, then recursing on left and right children, or terminating if it is a leaf or has value less than findMe
After testing I found this method is usually faster than the other 3 for searching. Before replacing vector access .at() with [ ] linearSearch was fastest.*/
bool MaxHeap::optimizedRecursiveSearch(const int findMe, std::size_t index /* = 1 */)
{
	if(heap[index] == findMe)
	{
		return true;
	}
	if(heap[index] < findMe || leftChildIndex(index) >= heap.size())
	{
		return false;
	}
	
	if(rightChildIndex(index) < heap.size())
	{
		if(optimizedRecursiveSearch(findMe, rightChildIndex(index))) return true;
	}
	
	return optimizedRecursiveSearch(findMe, leftChildIndex(index));
	

}

/*search based on extracting the max.*/
bool MaxHeap::extractMaxSearch(const int findMe)
{
	int max;
	MaxHeap mh;
	mh.buildHeap(heap);//build a copy heap to avoid side effects on original
	try
	{
		do
		{
			max = mh.extractMax();
			if(max == findMe) return max;
		}while(max > findMe);
	}
	catch(std::out_of_range const&)
	{
		return false;
	}
	return false;
}

/*return true if findMe is found*/
bool MaxHeap::search(const int findMe)
{
	if(heap.size() < 150)
	{
		return linearSearch(findMe);
	}
	else
	{
		return optimizedRecursiveSearch(findMe);
	}
}

/*Use formula floor(lg(n)) + 1 to determine depth of heap. Heap of 1 node has depth 1, 0 nodes is 0.
Recall first element is a dummy.*/
std::size_t MaxHeap::getDepth()
{
	if(heap.size() < 2) return 0;
	else return (floor(log2(heap.size() - 1)));
}

/*Return number of nodes in heap*/
std::size_t MaxHeap::getSize()
{
	return heap.size()-1;
}

/*delete node located at index i*/
void MaxHeap::deleteAtIndex(std::size_t index)
{
	index++;//account for data starting at index 1
	if(heap.back() > heap.at(index))
	{
		heap[index] = heap.back();
		heap.pop_back();
		siftUp(index);
	}
	else
	{
		heap[index] = heap.back();
		heap.pop_back();
		siftDown(index);
	}
}

bool MaxHeap::decreaseKey(std::size_t index, const int newVal)
{
	index++;//account for data starting at index 1
	if(index >= heap.size())
	{
		return false;
	}
	
	if(heap.at(index) < newVal)
	{
		return false;
	}
	
	heap[index] = newVal;
	siftDown(index);
	return true;
}

bool MaxHeap::increaseKey(std::size_t index, const int newVal)
{
	index++;//account for data starting at index 1
	if(index >= heap.size())
	{
		return false;
	}
	
	if(heap.at(index) > newVal)
	{
		return false;
	}
	
	heap[index] = newVal;
	siftUp(index);
	return true;	
}

/*more efficent than calling insert() and extractMax() separately*/
int MaxHeap::insertExtractMax(int insert)
{
	if(heap.empty())
	{
		throw std::out_of_range("Extraction failed, heap is empty");
	}
	
	
	if(heap[1] > insert)
	{
		std::swap(heap[1], insert);
		siftDown(1);
	}
	
	return insert;
}

/*more efficent than calling extractMax() and insert() separatley*/
int MaxHeap::extractMaxInsert(int insert)
{
	if(heap.empty())
	{
		throw std::out_of_range("Extraction failed, heap is empty");
	}
	
	std::swap(heap[1], insert);
	siftDown(1);
	return insert;
}

bool MaxHeap::isLeaf(const std::size_t index)
{
	return (index >= heap.size() / 2 + 1 && index < heap.size());
}