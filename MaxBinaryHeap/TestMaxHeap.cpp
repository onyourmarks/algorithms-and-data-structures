//compile with g++ TestMaxHeap.cpp -o TestMaxHeap.exe MaxHeap.cpp -Wall -Wextra -Wpedantic -Wconversion
#include <iostream>
#include <vector>
#include <chrono>
#include "MaxHeap.h"

void printDuration(const std::chrono::steady_clock::time_point t1, const std::chrono::steady_clock::time_point t2);

int main()
{
	/*can use this site to come up with test data <https://www.random.org/integer-sets/>*/
	std::vector<int> v7 = {8,5,10,234,234,1111,344,4454,3223,555334,};
	MaxHeap mh;
	std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
	mh.buildHeap(v7);
	std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
	std::cout << "duration of buildHeap ";
	printDuration(t1, t2);
	
	if(MaxHeap::isHeap(mh.toVector()))
	{
		std::cout << "is max binary heap" << std::endl;
	}
	else
	{
		std::cout << "is NOT max binary heap" << std::endl;
	}
	
	std::cout << "heap in vector form:\n";
	for(const auto i : mh.toVector())
		std::cout << i << ' ';
	std::cout << "\n\n";
	std::cout << "heap size is " << mh.getSize() << "\n";
	
		/*test inserting nodes*/
	int in = 0;
	while(in != -1)
	{
		std::cout << "Enter an integer to insert or -1 to terminate: ";
		std::cin >> in;
		mh.insert(in);
		std::cout << "heap size is now " << mh.getSize() << "\n";
	}
	
	
	/*test deleting nodes*/
	in = 0;
	while(true)
	{
		std::cout << "remove node from which index? ";
		std::cin >> in;
		if(in < 0)
		{
			break;
		}
		
		mh.deleteAtIndex(in);
		
		std::cout << "heap in vector form:\n";
		for(const auto i : mh.toVector())
		std::cout << i << ' ';
		std::cout << "\n\n";
		
		if(MaxHeap::isHeap(mh.toVector()))
		{
			std::cout << "\nis heap\n";
		}
		else
		{
			std::cout << "\nis NOT heap\n";
		}
		std::cout << "heap size is now " << mh.getSize() << "\n";
	}
	
	
	/*test inserting and extracting in one operation*/
	in = 0;
	while(in != -1)
	{
		std::cout << "InsertExtract: isnsert value? ";
		std::cin >> in;
	
		
		std::cout << "max (after insert): " << mh.insertExtractMax(in);
		
		std::cout << "\nheap in vector form:\n";
		for(const auto i : mh.toVector())
		std::cout << i << ' ';
		std::cout << "\n\n";
		
		if(MaxHeap::isHeap(mh.toVector()))
		{
			std::cout << "\nis heap\n";
		}
		else
		{
			std::cout << "\nis NOT heap\n";
		}
	}
	
	
	/*test extracting and inserting in one operation*/
	in = 0;
	while(in != -1)
	{
		std::cout << "ExtractInsert: insert value? ";
		std::cin >> in;
		
		std::cout << "max (before insert): " << mh.extractMaxInsert(in);
		
		std::cout << "\nheap in vector form:\n";
		for(const auto i : mh.toVector())
		std::cout << i << ' ';
		std::cout << "\n\n";
		
		if(MaxHeap::isHeap(mh.toVector()))
		{
			std::cout << "\nis heap\n";
		}
		else
		{
			std::cout << "\nis NOT heap\n";
		}
	}
	
	
	
	in = 1;
	/*test searching*/
	while(in != -1)
	{
		int findThis;
		std::cout << "What to find? ";
		std::cin >> findThis;
		bool linearFound, recursiveFound, optimizedRecursiveFound, extractMaxFound;
		
		t1 = std::chrono::steady_clock::now();
		linearFound = mh.linearSearch(findThis);
	    t2 = std::chrono::steady_clock::now();
		std::cout << "duration in nanoseconds of linearSearch ";
		printDuration(t1, t2);
		
		t1 = std::chrono::steady_clock::now();
		recursiveFound = mh.recursiveSearch(findThis);
		t2 = std::chrono::steady_clock::now();
		std::cout << "duration in nanoseconds of recursiveSearch ";
		printDuration(t1, t2);
		
		t1 = std::chrono::steady_clock::now();
		optimizedRecursiveFound = mh.optimizedRecursiveSearch(findThis);
		t2 = std::chrono::steady_clock::now();
		std::cout << "duration in nanoseconds of optimizedRecursiveSearch ";
		printDuration(t1, t2);
		
		t1 = std::chrono::steady_clock::now();
		extractMaxFound = mh.extractMaxSearch(findThis);
		t2 = std::chrono::steady_clock::now();
		std::cout << "duration in nanoseconds of extractMatchSearch ";
		printDuration(t1, t2);
		
		
		
		if(linearFound == recursiveFound && recursiveFound == optimizedRecursiveFound && optimizedRecursiveFound == extractMaxFound)
		{
			std::cout << "all searches agree\n";			
			if(linearFound)
			{
				std::cout << findThis << " was found\n\n";
			}
			else
			{
				std::cout << findThis << " was NOT found\n\n";
			}
		}
		else
		{
			std::cout << "NOT all searches agree\n";
			std::cout << "linearFound: " << linearFound << " recursiveFound: " << recursiveFound << " optimizedRecursiveFound: " << optimizedRecursiveFound << " extractMaxFound " << extractMaxFound << std::endl;
		}
		
	}
	
	
	return 0;
}

void printDuration(const std::chrono::steady_clock::time_point t1, const std::chrono::steady_clock::time_point t2)
{
	std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>( t2 - t1 ).count() << " nanoseconds\n";
}