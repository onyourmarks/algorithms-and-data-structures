#pragma once
#include <vector>

class MaxHeap
{
	public:
		/*create heap from vector*/
		void buildHeap(std::vector<int> data);
		
		/*return vector representation of heap*/
		std::vector<int> toVector();
		
		/*remove and return the max element of heap*/
		int extractMax();

		void insert(const int nodeVal);
		
		/*inserts then extracts max*/
		int insertExtractMax(int insert);
		
		/*extracts max then inserts*/
		int extractMaxInsert(int insert);
		
		void deleteAtIndex(const std::size_t index);
		
		/*return true if data is a binary max heap. Traversal of array takes place as breadth first.*/
		static bool isHeap(std::vector<int> data, const std::size_t startingIndex = 0);
		
		/*return true if index corresponds to a leaf*/
		bool isLeaf(const std::size_t index);
		
		std::size_t getDepth();
		std::size_t getSize();
		
		/*return true if findMe is in heap*/
		bool search(const int findMe);
		bool linearSearch(const int findMe);
		bool recursiveSearch(const int findMe, std::size_t index = 1);
		bool optimizedRecursiveSearch(const int findMe, std::size_t index = 1);
		bool extractMaxSearch(const int findMe);
		
		/*Decrease value at index to newVal. Return false and no change to heap if 1) index is out of range, or 2) current value is less than newVal*/
		bool decreaseKey(std::size_t index, const int newVal);
		
		/*Increase value at index to newVal. Return false and no change to heap if 1) index is out of range, or 2) current value is greater than newVal*/
		bool increaseKey(std::size_t index, const int newVal);
		
	
	private:		
		/*used for root removal and for creating from array*/
		void siftDown(const std::size_t node);//move an incorrectly placed node down
		
		/*used for inserts*/
		void siftUp(std::size_t nodeIndex);//move an incorrectly placed node up
		
		/*return indexes, not values*/
		static std::size_t parentIndex(const std::size_t node);
		static std::size_t leftChildIndex(const std::size_t node);
		static std::size_t rightChildIndex(const std::size_t node);
		
		int maxSize;
		int currentSize;
		std::vector<int> heap;
};