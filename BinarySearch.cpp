/*binary search: <https://en.wikipedia.org/wiki/Binary_search_algorithm#Alternative_procedure>*/
#include <iostream>
#include <cmath>

/*function declarations*/
int runTestCases();
template<typename T> int binarySearch(const T data[], const std::size_t length, const T findMe);//return -1 if not found

int main()
{
	auto failed = runTestCases();
	std::cout << "Failed tests: " << failed << std::endl;
	return 0;
}

template<typename T> int binarySearch(const T data[], const std::size_t length, const T findMe)
{
	std::size_t left = 0;
	std::size_t right = length - 1;
	while(left != right)
	{
		const auto mid = left + static_cast<std::size_t>(ceil((right - left) / 2.0));
		if(data[mid] > findMe)
		{
			right = mid - 1;
		}
		else
		{
			left = mid;
		}
	}
	
	if(data[left] == findMe)
	{
		return left;
	}
	
	return -1;
}

int runTestCases()
{
	auto failed = 0;
	
	int test1[1] = {1};
	int test2[7] = {1,2,3,4,5,6,7};
	int test3[8] = {1,2,3,4,5,6,7,8};
	int test4[7] = {1,2,3,4,5,7,7};
	int test5[8] = {1,2,3,4,5,7,8,8};
	int test6[3] = {1,2,3};
	int test7[3] = {1,1,1};
	int test8[5] = {-10, -2, 5, 6, 10};
	float test9[6] = {1.1, 1.2, 1.3, 1.8, 2.0, 2.5};
	
	int result;
	result = binarySearch(test1, 1, 1);
	if(test1[result] != 1)
	{
		std::cerr << "Test A failed. Expected 0, got " << result << "\n";
		failed++;
	}
	
	result = binarySearch(test2, 7, 3);
	if(test2[result] != 3)
	{
		std::cerr << "Test B failed. Expected 2, got " << result << "\n";
		failed++;
	}
	
	result = binarySearch(test2, 7, 1);
	if(test2[result] != 1)
	{
		std::cerr << "Test C failed. Expected 0, got " << result << "\n";
		failed++;
	}
	
	result = binarySearch(test2, 7, 2);
	if(test2[result] != 2)
	{
		std::cerr << "Test D failed. Expected 1, got " << result << "\n";
		failed++;
	}
	
	result = binarySearch(test3, 8, 5);
	if(result != 4)
	{
		std::cerr << "Test E failed. Expected 4, got " << result << "\n";
		failed++;
	}
	
	result = binarySearch(test4, 7, 7);
	if(test4[result] != 7)
	{
		std::cerr << "Test F failed. Expected 5, got " << result << "\n";
		failed++;
	}
	
	result = binarySearch(test5, 8, 8);
	if(result != 6 && result != 7)
	{
		std::cerr << "Test G failed. Expected 6 or 7, got " << result << "\n";
		failed++;
	}
	
	result = binarySearch(test6, 3, 1);
	if(result != 0)
	{
		std::cerr << "Test H failed. Expected 0, got " << result << "\n";
		failed++;
	}
	
	result = binarySearch(test7, 3, 1);
	if(result != 0 && result != 1 && result != 2)
	{
		std::cerr << "Test I failed. Expected 0 or 1, got " << result << "\n";
		failed++;
	}
	
	result = binarySearch(test8, 3, 0);
	if(result != -1)
	{
		std::cerr << "Test J failed. Expected -1, got " << result << "\n";
		failed++;
	}
	
	result = binarySearch(test8, 5, -10);
	if(test8[result] != -10)
	{
		std::cerr << "Test K failed. Expected 0, got " << result << "\n";
		failed++;
	}
	
	result = binarySearch(test8, 5, -2);
	if(test8[result] != -2)
	{
		std::cerr << "Test L failed. Expected 1, got " << result << "\n";
		failed++;
	}
	
	result = binarySearch(test8, 5, 6);
	if(test8[result] != 6)
	{
		std::cerr << "Test M failed. Expected 3, got " << result << "\n";
		failed++;
	}
	
	result = binarySearch(test9, 6, 1.3f);
	if(test9[result] != 1.3f)
	{
		std::cerr << "Test N failed. Expected 2, got " << result << "\n";
		failed++;
	}
	
	result = binarySearch(test9, 6, 1.1f);
	if(test9[result] != 1.1f)
	{
		std::cerr << "Test O failed. Expected 2, got " << result << "\n";
		failed++;
	}
	
	result = binarySearch(test9, 6, 2.5f);
	if(test9[result] != 2.5f)
	{
		std::cerr << "Test P failed. Expected 5, got " << result << "\n";
		failed++;
	}
	
	
	
	
	return failed;
}