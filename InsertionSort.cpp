/*insertion sort: <https://en.wikipedia.org/wiki/Insertion_sort#Algorithm>*/
#include <iostream>

/*function declerations*/
int runTestCases();
template<typename T> void insertionSort(T data[], std::size_t len);
template<typename T> bool checkAscending(T data[], std::size_t len);
template<typename T> void printArr(T arr[], std::size_t len, char pre='\0', char post='\0');


int main()
{
	const auto failed = runTestCases();
	std::cout << "Number of failed tests: " << failed << " out of 10" << std::endl;
	return failed;
}

template<typename T> void insertionSort(T data[], std::size_t len)
{	
	for(std::size_t i = 1; i < len; i++)
	{
		const auto x = data[i];
		std::size_t j;
		for(j = i - 1; j + 1  > 0 && data[j] > x; j--)
		{
			data[j+1] = data[j];
		}
		data[j+1] = x;
	}
}

int runTestCases()
{
	int test1[6] = {2,4,9,6,5,1};
	int test2[6] = {1,2,3,4,5,6};
	int test3[6] = {6,5,4,3,2,1};
	int test4[6] = {-6,5,4,3,2,-1};
	int test5[6] = {1,1,1,1,1,1};
	int test6[6] = {1,1,1,1,1,2};
	int test7[6] = {1,2,1,1,1,1};
	int test8[1] = {5};
	int test9[2] = {9,4};
	int test10[2] = {4,9};
	float test11[5] = {1.1f, -1.3f, 5.0f, 7.6f, 7.0f};
	
	std::cout << "Three examples for visual inspection:\n";
	printArr(test1, 6, '\t', '\n');
	printArr(test2, 6, '\t', '\n');
	printArr(test11, 5, '\t', '\n');

	insertionSort(test1, 6);
	insertionSort(test2, 6);
	insertionSort(test3, 6);
	insertionSort(test4, 6);
	insertionSort(test5, 6);
	insertionSort(test6, 6);
	insertionSort(test7, 6);
	insertionSort(test8, 1);
	insertionSort(test9, 2);
	insertionSort(test10, 2);
	insertionSort(test11, 5);
	
	auto failed = 0;
	
	
	if(!checkAscending(test1, 6))
	{
		std::cerr << "Test A failed\n";
		failed++;
	}
	
	if(!checkAscending(test2, 6))
	{
		std::cerr << "Test B failed\n";
		failed++;
	}
	
	if(!checkAscending(test3, 6))
	{
		std::cerr << "Test C failed\n";
		failed++;
	}
	
	if(!checkAscending(test4, 6))
	{
		std::cerr << "Test D failed\n";
		failed++;
	}
	
	if(!checkAscending(test5, 6))
	{
		std::cerr << "Test E failed\n";
		failed++;
	}
	
	if(!checkAscending(test6, 6))
	{
		std::cerr << "Test F failed\n";
		failed++;
	}
	
	if(!checkAscending(test7, 6))
	{
		std::cerr << "Test G failed\n";
		failed++;
	}
	
	if(!checkAscending(test8, 1))
	{
		std::cerr << "Test H failed\n";
		failed++;
	}
	
	if(!checkAscending(test9, 2))
	{
		std::cerr << "Test I failed\n";
		failed++;
	}
	
	if(!checkAscending(test10, 2))
	{
		std::cerr << "Test J failed\n";
		failed++;
	}
	
	if(!checkAscending(test11, 5))
	{
		std::cerr << "Test K failed\n";
		failed++;
	}
	
	std::cout << "Now sorted:\n";
	printArr(test1, 6, '\t', '\n');
	printArr(test2, 6, '\t', '\n');
	printArr(test11, 5, '\t', '\n');
	
	
	return failed;
}

/*Return true if data is sorted in ascending order, false otherwise*/
template<typename T> bool checkAscending(T data[], std::size_t len)
{
	if(len < 2)
	{
		return true;
	}
	for(std::size_t i = 0; i < len - 1; i++)
	{
		if(data[i] > data[i + 1])
		{
			return false;
		}
	}
	return true;
}

/*Prints contents of array to standard output. Optionally specify start and end characters for delamination (e.g. new line).*/
template <typename T> void printArr(T arr[], std::size_t len, char pre, char post)
{
	std::cout << pre;
	for(std::size_t i = 0; i < len - 1; i++)
	{
		std::cout << arr[i] << ", ";
	}
	std::cout<< arr[len - 1];
	std::cout << post;
}